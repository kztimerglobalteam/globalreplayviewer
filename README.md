GlobalReplayViewer
=========

This is a GOKZ replay viewer made with Aurelia and based off of [Zik's GokzReplayViewer](https://github.com/Metapyziks/GOKZReplayViewer)

[Online Demo](https://replays.jsachs.net/watch/record/859026)

This is a work in progress

### About ###

This project uses the GlobalAPI to load demo data.
The map resources are hosted by [Ruto](https://rutoc.me/), newers maps may need to be exported using [Zik's SourceUtils Export Tool](https://github.com/Metapyziks/SourceUtils).

### Getting started ###

Clone this repository, go to the root folder install all dependencies using Yarn.

```console
yarn install
```

If you don't have yarn installed, just simply run:

```console
npm i -g yarn
```

Once dependencies are installed, you can start developing by running

```console
au run --watch
```

If you run into an error, you may need to install `aurelia-cli` as a global npm package like so:

```console
npm i -g aurelia-cli
```
If you want to build for production, you need to run

```console
au build --env prod
```

The WebPack bundle will be on the `/dist` folder.

### TODO ###

* Get top records and grab the `replay_id`
* Add an UI to show the Player's name, avatar, game mode.
* Add loaders, as of know it's unclear if it's loading or failed competely.
* Make everything user friendly
* Add sound, maybe?
* Fix various shader issues (fog, smoke and particles does not render properly)
