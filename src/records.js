import { LogManager } from 'aurelia-framework';
const log = LogManager.getLogger('RecordsView');

import { RecordsAPI } from './service/global-api/records';

export class Records {
  static inject = [RecordsAPI];
  constructor(recordsApi) {
    this.recordsApi = recordsApi;
    this.records = {
      kz_timer: [],
      kz_simple: [],
      vanilla: []
    };
  }
  async activate(params, routeConfig, navigationInstruction) {
    this.loadRecords();
  }
  async loadRecords() {
    try {
      let kztimerRecords = await this.recordsApi.getTopRecords('kz_timer', 128, 10);
      log.debug(kztimerRecords);
    } catch (e) {
      log.error(e);
    }
  }
}
