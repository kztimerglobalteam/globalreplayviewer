import { LogManager } from 'aurelia-framework';
const log = LogManager.getLogger('Watch-Replay');

export class WatchReplay {
  constructor() {
    this.recordId = null;
    this.replayId = null;
  }
  activate(params, routeConfig, navigationInstruction) {
    this.params = params;
    if (!this.params.replayId && !this.params.recordId) {
      alert('No replay or record ID were given!');
    }
    if (this.params.recordId) this.recordId = this.params.recordId;
    if (this.params.replayId) this.replayId = this.params.replayId;
  }
}
