/**
 * Aries Auth Service
 */

import { LogManager } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { EventAggregator } from 'aurelia-event-aggregator';
import Config from 'configuration';
import qs from 'querystring';

const log = LogManager.getLogger('GlobalAPI-Records');

const KZModes = {
  kz_timer: 'kz_timer',
  kz_simple: 'kz_simple',
  vanilla: 'vanilla'
};

export class RecordsAPI {
  static inject = [EventAggregator];
  constructor(eventAggregator) {
    this.eventAggregator = eventAggregator;
    this.http = new HttpClient();
    this.http.configure(config => {
      config.withBaseUrl(`${Config.api.base}/records/`);
      config.useStandardConfiguration();
      config.withInterceptor({
        requestError: error => {
          log.error('Request', error);
        },
        responseError: error => {
          log.error('Response', error);
        }
      });
    });
  }

  /**
   * Gets the latest world records
   * @param {KZModes} modes_list_string "KZ Movement Mode"
   * @param {number} tickrate "Server tickrate"
   * @param {number} limit "Records limit"
   */
  async getTopRecords(modes_list_string = KZModes.kz_simple, tickrate = 128, limit = 10) {
    try {
      let request = await this.http.fetch(`top?${qs.stringify({modes_list_string, tickrate, limit})}`, {
        method: 'GET'
      });
      let response = await request.json();
      if (response.success) {
        return response;
      }
      if (!response.success) {
        log.error(response);
        return {error: true, message: response.message};
      }
    } catch (error) {
      log.error(error);
      return {error: true, message: 'Something went wrong, check the logs'};
    }
  }
}
