import './sass/app.scss';
import { PLATFORM } from 'aurelia-framework';

export class App {
  configureRouter(config, router) {
    this.router = router;
    config.title = 'GlobalAPI | Replays';
    config.options.pushState = true;
    config.options.root = '/';
    config.map([
      { route: ['', 'records'], name: 'records', moduleId: PLATFORM.moduleName('records'), nav: true },
      { route: ['replay/:replayId', 'record/:recordId'], name: 'watchReplay', moduleId: PLATFORM.moduleName('watch-replay'), nav: false }
    ]);
  }
}
