import { bindable } from 'aurelia-framework';
import { LogManager } from 'aurelia-framework';
const log = LogManager.getLogger('KZ-Replay-Viewer');

import { ReplayViewer } from './zikviewer/ReplayViewer';
import { CameraMode } from './zikviewer/SourceUtils/MapViewer';

import Configuration from '../../configuration';

export class KzReplayViewer {
  @bindable replayId = null;
  @bindable recordId = null;

  attached() {
    this.viewer = new ReplayViewer(this.viewerDom);

    this.viewer.cameraMode = CameraMode.Fixed;
    this.viewer.mapBaseUrl = Configuration.api.mapBase;

    this.viewer.isPlaying = true;
    this.viewer.showDebugPanel = true;

    if (this.replayId || this.recordId) {
      let demoUrl = `${Configuration.api.base}/records`;
      if (this.replayId) {
        demoUrl = `${demoUrl}/replay/${this.replayId}`;
      } else {
        demoUrl = `${demoUrl}/${this.recordId}/replay`;
      }
      this.viewer.loadReplay(demoUrl);
      this.viewer.animate();
    } else {
      log.warn('Invalid replay id or was not given.');
    }
  }
}
